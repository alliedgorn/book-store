# Book Store REST API 

This is a REST service that provides a demonstration of creating a simple book store API with security layer.

## Build and Run

Use Gradle:

```sh
PORT="" DB_URL="" DB_USERNAME="" DB_PASSWORD="" gradle clean build bootRun
```

These are some required environment variables need for running this application. You must supply values of these variables.

```sh
# Example
PORT="8081" # Specify port number for running aplication
DB_URL="jdbc:mysql://localhost:32768/bookstore?useSSL=false" # Specify database connection used
DB_USERNAME="root" # A username to access the database
DB_PASSWORD="1234" # A password to access the database
```

## Usage

Test the `books` endpoint:

```sh
curl http://localhost:8081/api/books
```

You receive the following JSON response:

```json
[
  {
    "author": "Annejet van der Zijl, Michele Hutchison",
    "id": 5,
    "is_recommended": true,
    "name": "An American Princess: The Many Lives of Allene Tew",
    "price": 149.0
  },
  {
    "author": "Kristin Hannah",
    "id": 4,
    "is_recommended": true,
    "name": "The Great Alone: A Novel Kristin Hannah",
    "price": 495.0
  },
  ...
]
```

In order to access the protected resource, you must first request an access token via:

```sh
curl -X POST -i http://localhost:8081/api/login -H 'Content-Type: application/json' -d '{"username": "username", "password": "password"}'
```

A successful authorization results in the following response:

```sh
HTTP/1.1 200 
Authorization: BookStore eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJnb3JuLmtvbmcxIiwiZXhwIjoxNTMzODcxNzgxfQ.62P6B8HWUxJxAMPsXjpQsLJ3sLH7rUnT6YQWaGH_nsPB2S3iQsnHWLxc2ZXczuABPahmyJrdSj_waKeut21u_Q
X-Content-Type-Options: nosniff
X-XSS-Protection: 1; mode=block
Cache-Control: no-cache, no-store, max-age=0, must-revalidate
Pragma: no-cache
Expires: 0
X-Frame-Options: DENY
Content-Length: 0
Date: Tue, 31 Jul 2018 03:29:41 GMT
```

Use the value in `Authorization` returned in the previous request to make the authorized request to the protected endpoint:

```sh
curl http://localhost:8081/api/users -H "Authorization: BookStore eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJnb3JuLmtvbmcxIiwiZXhwIjoxNTMzODcxNzgxfQ.62P6B8HWUxJxAMPsXjpQsLJ3sLH7rUnT6YQWaGH_nsPB2S3iQsnHWLxc2ZXczuABPahmyJrdSj_waKeut21u_Q"
```

If the request is successful, you will see the following JSON response:

```json
{
  "books": [],
  "date_of_birth": "01/01/1985",
  "name": "name",
  "surname": "surname"
}
```

For API documents please contact this URL

```
http://localhost:8081/swagger-ui.html
```