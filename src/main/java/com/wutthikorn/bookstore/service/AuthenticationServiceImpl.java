package com.wutthikorn.bookstore.service;

import com.wutthikorn.bookstore.domain.User;
import com.wutthikorn.bookstore.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    @Override
    public Boolean check(String username, String password) {

        Optional<User> user = userRepository.findByUsername(username);

        if (!user.isPresent()) {
            throw new UsernameNotFoundException(String.format("Could not find user with username %s", username));
        }

        return passwordEncoder.matches(password, user.get().getPassword());
    }
}
