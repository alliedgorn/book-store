package com.wutthikorn.bookstore.service;

import com.wutthikorn.bookstore.exception.ResourceNotFoundException;
import com.wutthikorn.bookstore.view.OrderDto;
import com.wutthikorn.bookstore.view.OrderSuccessDto;

public interface OrderService {
    OrderSuccessDto order(OrderDto order, String username) throws ResourceNotFoundException;
}
