package com.wutthikorn.bookstore.service;

import com.wutthikorn.bookstore.domain.Book;
import com.wutthikorn.bookstore.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    @Override
    public List<Book> getAll() {
        return bookRepository.getAllSorted();
    }
}
