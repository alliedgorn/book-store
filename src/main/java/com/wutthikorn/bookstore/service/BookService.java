package com.wutthikorn.bookstore.service;

import com.wutthikorn.bookstore.domain.Book;

import java.util.List;

public interface BookService {
    List<Book> getAll();
}
