package com.wutthikorn.bookstore.service;

import org.springframework.stereotype.Service;

@Service
public interface AuthenticationService {

    Boolean check(String username, String password);
}
