package com.wutthikorn.bookstore.service;

import com.wutthikorn.bookstore.domain.User;
import com.wutthikorn.bookstore.view.UserDto;

public interface UserService {
    void createUser(User user);

    UserDto getUser(String username);

    void deleteUser(String username);
}
