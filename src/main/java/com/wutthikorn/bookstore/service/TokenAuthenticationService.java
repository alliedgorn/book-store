package com.wutthikorn.bookstore.service;

import com.wutthikorn.bookstore.domain.User;
import com.wutthikorn.bookstore.repository.UserRepository;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.Optional;

import static java.util.Collections.emptyList;

@Service
@RequiredArgsConstructor
public class TokenAuthenticationService {

    @Value("${security.jwt.expiration}")
    private long expiration;

    @Value("${security.jwt.secret}")
    private String secret;

    @Value("${security.jwt.prefix}")
    private String tokenPrefix;

    private final UserRepository userRepository;

    private static final String HEADER_STRING = "Authorization";

    public void addAuthentication(HttpServletResponse res, String username) {
        String jwt = Jwts.builder()
            .setSubject(username)
            .setExpiration(new Date(System.currentTimeMillis() + expiration))
            .signWith(SignatureAlgorithm.HS512, secret)
            .compact();
        res.addHeader(HEADER_STRING, tokenPrefix + " " + jwt);
    }

    public Optional<Authentication> getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HEADER_STRING);
        if (token != null) {
            String user = Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token.replace(tokenPrefix, ""))
                .getBody()
                .getSubject();
            Optional<User> userOptional = userRepository.findByUsername(user);
            return userOptional.isPresent() ?
                Optional.of(new UsernamePasswordAuthenticationToken(user, null, emptyList())) :
                Optional.empty();
        }
        return Optional.empty();
    }
}