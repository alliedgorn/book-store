package com.wutthikorn.bookstore.service;

import com.wutthikorn.bookstore.domain.Order;
import com.wutthikorn.bookstore.domain.OrderLine;
import com.wutthikorn.bookstore.domain.User;
import com.wutthikorn.bookstore.repository.OrderRepository;
import com.wutthikorn.bookstore.repository.UserRepository;
import com.wutthikorn.bookstore.view.UserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final OrderRepository orderRepository;

    private final PasswordEncoder passwordEncoder;

    @Override
    public void createUser(User user) {
        if (usernameExists(user.getUsername())) {
            throw new IllegalArgumentException("Username already exists");
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    @Override
    public UserDto getUser(String username) {

        Optional<User> userOptional = userRepository.findByUsername(username);

        if (!userOptional.isPresent()) {
            throw new UsernameNotFoundException(String.format("Could not find username %s", username));
        }

        List<Order> orders = orderRepository.findByUserID(userOptional.get().getUserID());

        return createUserDto(userOptional.get(), orders);
    }

    @Override
    public void deleteUser(String username) {

        Optional<User> userOptional = userRepository.findByUsername(username);

        if (!userOptional.isPresent()) {
            throw new UsernameNotFoundException(String.format("Could not find username %s", username));
        }

        List<Order> orders = orderRepository.findByUserID(userOptional.get().getUserID());

        userRepository.delete(userOptional.get());
        orderRepository.deleteAll(orders);

    }

    private List<Long> extractBookIDFromOrderList(List<Order> orders) {
        return orders.stream()
            .map(Order::getOrderLines)
            .flatMap(List::stream)
            .map(OrderLine::getBookID)
            .distinct()
            .collect(Collectors.toList());
    }

    private UserDto createUserDto(User user, List<Order> orders) {

        return new UserDto()
            .setName(user.getName())
            .setSurname(user.getSurname())
            .setDateOfBirth(user.getDateOfBirth())
            .setBooks(extractBookIDFromOrderList(orders));
    }

    private Boolean usernameExists(String username) {
        return userRepository.findByUsername(username).isPresent();
    }
}
