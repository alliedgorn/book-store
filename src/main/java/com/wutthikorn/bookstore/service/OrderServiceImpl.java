package com.wutthikorn.bookstore.service;

import com.wutthikorn.bookstore.domain.Book;
import com.wutthikorn.bookstore.domain.Order;
import com.wutthikorn.bookstore.domain.OrderLine;
import com.wutthikorn.bookstore.domain.User;
import com.wutthikorn.bookstore.exception.ResourceNotFoundException;
import com.wutthikorn.bookstore.repository.BookRepository;
import com.wutthikorn.bookstore.repository.OrderRepository;
import com.wutthikorn.bookstore.repository.UserRepository;
import com.wutthikorn.bookstore.view.OrderDto;
import com.wutthikorn.bookstore.view.OrderSuccessDto;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    private final BookRepository bookRepository;

    private final UserRepository userRepository;

    @Override
    @Transactional
    public OrderSuccessDto order(OrderDto orderDto, String username) throws ResourceNotFoundException {

        Optional<User> userOptional = userRepository.findByUsername(username);

        if (!userOptional.isPresent()) {
            throw new UsernameNotFoundException(String.format("Could not find username %s", username));
        }

        final Order order = new Order().setUserID(userOptional.get().getUserID());

        List<Book> books = getBookFromOrderDto(orderDto);

        List<OrderLine> orderLines = books.stream().map(
            book -> new OrderLine()
                .setPrice(BigDecimal.valueOf(book.getPrice()))
                .setBookID(book.getId())
                .setName(book.getName()))
            .collect(Collectors.toList());

        order.setOrderLines(orderLines);

        orderRepository.save(order);

        return new OrderSuccessDto().setPrice(order.calculateTotal());

    }

    private List<Book> getBookFromOrderDto(OrderDto orderDto) throws ResourceNotFoundException {
        List<Optional<Book>> books = orderDto.getOrders().stream()
            .map(bookRepository::findByID)
            .collect(Collectors.toList());

        Boolean hasMissingBooks = books.stream()
            .map(book -> !book.isPresent())
            .reduce(false, (res, cur) -> cur || res);

        if (hasMissingBooks) {
            throw new ResourceNotFoundException("There might be some incorrect items in this order");
        }

        return books.stream().filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList());
    }

}
