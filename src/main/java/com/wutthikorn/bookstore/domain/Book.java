package com.wutthikorn.bookstore.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.Table;
import java.util.Objects;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@Table(name = "BOOKS")
public class Book {

    private Long id;

    private String name;

    private String author;

    private Double price;

    private Boolean isRecommended;

    @Override
    public boolean equals(Object o) {

        if (o == this) return true;
        if (!(o instanceof Book)) {
            return false;
        }
        Book book = (Book) o;
        return name.equals(book.name) &&
            author.equals(book.author) &&
            price.equals(book.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, author, price);
    }

}
