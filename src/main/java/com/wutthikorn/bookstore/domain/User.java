package com.wutthikorn.bookstore.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
@Table(name = "USERS")
public class User {

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "USER_ID", columnDefinition = "bigint", nullable = false)
    private Long userID;

    @Column(name = "USERNAME", columnDefinition = "varchar(255)", nullable = false)
    private String username;

    @Column(name = "PASSWORD", columnDefinition = "varchar(255)", nullable = false)
    private String password;

    @Column(name = "NAME", columnDefinition = "varchar(255)", nullable = false)
    private String name;

    @Column(name = "SURNAME", columnDefinition = "varchar(255) default ''", nullable = false)
    private String surname;

    @Column(name = "DATE_OF_BIRTH", columnDefinition = "date", nullable = false)
    private LocalDate dateOfBirth;

}
