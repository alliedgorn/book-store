package com.wutthikorn.bookstore.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
@Table(name = "ORDER_LINES")
public class OrderLine {

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ORDER_LINE_ID", columnDefinition = "bigint", nullable = false)
    private Long orderLineID;

    @Column(name = "BOOK_ID", columnDefinition = "bigint", nullable = false)
    private Long bookID;

    @Column(name = "NAME", columnDefinition = "varchar(255)", nullable = false)
    private String name;

    @Column(name = "PRICE", columnDefinition = "decimal(10,2)", nullable = false)
    private BigDecimal price;
}
