package com.wutthikorn.bookstore.repository.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wutthikorn.bookstore.domain.Book;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Objects;

@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class ScbTestBook {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("author_name")
    private String authorName;

    @JsonProperty("book_name")
    private String bookName;

    @JsonProperty("price")
    private Double price;

    public Boolean isContainedIn(List<ScbTestBook> bookList) {
        return bookList.stream().anyMatch(this::equals);
    }

    public Book toBook() {
        return new Book()
            .setId(this.id)
            .setAuthor(this.authorName)
            .setName(this.bookName)
            .setPrice(this.price);
    }

    @Override
    public boolean equals(Object o) {

        if (o == this) return true;
        if (!(o instanceof ScbTestBook)) {
            return false;
        }
        ScbTestBook book = (ScbTestBook) o;
        return authorName.equals(book.authorName) &&
            bookName.equals(book.bookName) &&
            price.equals(book.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(authorName, bookName, price);
    }
}
