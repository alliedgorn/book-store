package com.wutthikorn.bookstore.repository;

import com.wutthikorn.bookstore.domain.Book;

import java.util.List;
import java.util.Optional;

public interface BookRepository {
    List<Book> getAllSorted();

    Optional<Book> findByID(Long bookID);
}
