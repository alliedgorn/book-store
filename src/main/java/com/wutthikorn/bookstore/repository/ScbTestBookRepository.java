package com.wutthikorn.bookstore.repository;

import com.wutthikorn.bookstore.domain.Book;
import com.wutthikorn.bookstore.repository.model.ScbTestBook;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class ScbTestBookRepository implements BookRepository {

    private final RestTemplate restTemplate;

    @Override
    public List<Book> getAllSorted() {
        List<ScbTestBook> books = getAllBooks();
        List<ScbTestBook> recommendedBooks = getRecommendedBooks();

        return books.stream()
            .map(scbTestBook -> scbTestBook.toBook()
                .setIsRecommended(scbTestBook
                    .isContainedIn(recommendedBooks)))
            .sorted(Comparator
                .comparing(Book::getIsRecommended)
                .reversed()
                .thenComparing(Book::getName))
            .distinct()
            .collect(Collectors.toList());
    }

    @Override
    public Optional<Book> findByID(Long bookID) {
        return getAllBooks()
            .stream()
            .filter(book -> book.getId().equals(bookID))
            .map(ScbTestBook::toBook)
            .findFirst();
    }

    private List<ScbTestBook> getRecommendedBooks() {
        return restTemplate.exchange(
            "https://scb-test-book-publisher.herokuapp.com/books/recommendation",
            HttpMethod.GET,
            null,
            new ParameterizedTypeReference<List<ScbTestBook>>() {
            }).getBody();
    }

    private List<ScbTestBook> getAllBooks() {
        return restTemplate.exchange(
            "https://scb-test-book-publisher.herokuapp.com/books",
            HttpMethod.GET,
            null,
            new ParameterizedTypeReference<List<ScbTestBook>>() {
            }).getBody();
    }
}
