package com.wutthikorn.bookstore.repository;

import com.wutthikorn.bookstore.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> findByUserID(Long userID);
}
