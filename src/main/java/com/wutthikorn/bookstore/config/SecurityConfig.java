package com.wutthikorn.bookstore.config;

import com.wutthikorn.bookstore.filter.JWTAuthenticationFilter;
import com.wutthikorn.bookstore.filter.JWTLoginFilter;
import com.wutthikorn.bookstore.service.TokenAuthenticationService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final TokenAuthenticationService tokenAuthenticationService;

    private final AuthenticationProvider authProvider;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable().authorizeRequests()
            .antMatchers(HttpMethod.GET, "/api/books").permitAll()
            .antMatchers(HttpMethod.POST, "/api/users").permitAll()
            .antMatchers(HttpMethod.GET, "/api/users").authenticated()
            .antMatchers(HttpMethod.DELETE, "/api/users").authenticated()
            .antMatchers(HttpMethod.POST, "/api/users/orders").authenticated()
            .antMatchers(HttpMethod.POST, "/api/login").authenticated()
            .and()
            .addFilterBefore(new JWTLoginFilter("/api/login", authenticationManager(), tokenAuthenticationService),
                UsernamePasswordAuthenticationFilter.class)
            .addFilterBefore(new JWTAuthenticationFilter(tokenAuthenticationService),
                UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider);
    }


}
