package com.wutthikorn.bookstore.filter;


import com.wutthikorn.bookstore.service.TokenAuthenticationService;
import io.jsonwebtoken.JwtException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@RequiredArgsConstructor
public class JWTAuthenticationFilter extends GenericFilterBean {

    private final TokenAuthenticationService tokenAuthenticationService;

    @Override
    public void doFilter(ServletRequest request,
                         ServletResponse response,
                         FilterChain filterChain)
        throws IOException, ServletException {
        try {
            Optional<Authentication> authentication = tokenAuthenticationService
                .getAuthentication((HttpServletRequest) request);

            authentication.ifPresent(authentication1 -> SecurityContextHolder.getContext()
                .setAuthentication(authentication1));
            filterChain.doFilter(request, response);
        } catch (JwtException ex) {
            ((HttpServletResponse) response).setStatus(500);
        }
    }
}