package com.wutthikorn.bookstore.web;

import com.wutthikorn.bookstore.service.BookService;
import com.wutthikorn.bookstore.view.BookDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;

    @RequestMapping(method = RequestMethod.GET,
        path = "/api/books",
        produces = MediaType.APPLICATION_JSON_VALUE)
    public List<BookDto> getBooks() {
        return bookService.getAll()
            .stream()
            .map(BookDto::parse)
            .collect(Collectors.toList());
    }
}
