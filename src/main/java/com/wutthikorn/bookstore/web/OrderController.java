package com.wutthikorn.bookstore.web;

import com.wutthikorn.bookstore.exception.ResourceNotFoundException;
import com.wutthikorn.bookstore.service.OrderService;
import com.wutthikorn.bookstore.view.OrderDto;
import com.wutthikorn.bookstore.view.OrderSuccessDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;

    @RequestMapping(method = RequestMethod.POST,
        path = "/api/users/orders",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity order(@Valid @RequestBody OrderDto orderDto, Principal principal) {

        try {
            OrderSuccessDto result = orderService.order(orderDto, principal.getName());
            return ResponseEntity.status(200).body(result);

        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(400).build();
        }
    }

}
