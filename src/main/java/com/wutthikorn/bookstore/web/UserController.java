package com.wutthikorn.bookstore.web;

import com.wutthikorn.bookstore.service.UserService;
import com.wutthikorn.bookstore.view.CreateUserDto;
import com.wutthikorn.bookstore.view.UserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @RequestMapping(method = RequestMethod.POST,
        path = "/api/users",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createUser(@Valid @RequestBody CreateUserDto createUserDto) {

        userService.createUser(createUserDto.toUser());

        return ResponseEntity.status(200).build();
    }

    @RequestMapping(method = RequestMethod.GET,
        path = "/api/users",
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getUser(Principal principal) {

        UserDto result = userService.getUser(principal.getName());

        return ResponseEntity.status(200).body(result);
    }

    @RequestMapping(method = RequestMethod.DELETE,
        path = "/api/users")
    public ResponseEntity deleteUser(Principal principal) {

        userService.deleteUser(principal.getName());

        return ResponseEntity.status(200).build();
    }
}
