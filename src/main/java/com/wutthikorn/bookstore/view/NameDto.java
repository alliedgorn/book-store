package com.wutthikorn.bookstore.view;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class NameDto {
    private String name;
    private String surname;
}
