package com.wutthikorn.bookstore.view;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wutthikorn.bookstore.domain.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateUserDto {

    @NotNull
    @Length(min = 6, max = 20)
    private String username;

    @NotNull
    @Length(min = 10, max = 30)
    private String password;

    @NotNull
    @JsonProperty("date_of_birth")
    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate dateOfBirth;

    public User toUser() {
        NameDto nameDto = convertUserNameToName();
        return new User()
            .setUsername(username)
            .setPassword(password)
            .setName(nameDto.getName())
            .setSurname(nameDto.getSurname())
            .setDateOfBirth(dateOfBirth);
    }

    private NameDto convertUserNameToName() {
        List<String> fragments = new ArrayList<>(Arrays.asList(username.split("\\.")));
        String name = fragments.get(0);
        fragments.remove(0);
        return new NameDto(name, String.join(".", fragments));
    }
}
