package com.wutthikorn.bookstore.view;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wutthikorn.bookstore.domain.Book;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class BookDto {

    private Long id;

    private String name;

    private String author;

    private Double price;

    @JsonProperty("is_recommended")
    private Boolean isRecommended;

    public static BookDto parse(Book book) {
        return new BookDto(book.getId(),
            book.getName(),
            book.getAuthor(),
            book.getPrice(),
            book.getIsRecommended());
    }
}
