package com.wutthikorn.bookstore.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrderTest {

    private Order cut;

    @Mock
    private OrderLine orderLine1;

    @Mock
    private OrderLine orderLine2;

    private final Fixture fixture = new Fixture();

    @Before
    public void init() {
        cut = new Order().setOrderLines(asList(orderLine1, orderLine2));
    }

    @Test
    public void calculateTotalShouldReturnCorrectValue() {

        when(orderLine1.getPrice()).thenReturn(fixture.price1);
        when(orderLine2.getPrice()).thenReturn(fixture.price2);

        BigDecimal result = cut.calculateTotal();

        assertThat(result).isEqualTo(fixture.price1.add(fixture.price2));
    }

    private class Fixture {
        BigDecimal price1 = new BigDecimal(500.50);
        BigDecimal price2 = new BigDecimal(132.43);
    }

}