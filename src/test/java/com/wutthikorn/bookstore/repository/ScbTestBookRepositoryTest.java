package com.wutthikorn.bookstore.repository;

import com.wutthikorn.bookstore.domain.Book;
import com.wutthikorn.bookstore.repository.model.ScbTestBook;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ScbTestBookRepositoryTest {

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private RestTemplate restTemplate;

    private BookRepository cut;

    private Fixtures fixtures = new Fixtures();

    @Before
    public void init() {
        cut = new ScbTestBookRepository(restTemplate);
    }

    @Test
    public void getAllSortedShouldReturnAllBookWithCorrectRecommendation() {
        when(restTemplate
            .exchange(
                eq("https://scb-test-book-publisher.herokuapp.com/books"),
                any(), any(), ArgumentMatchers.<ParameterizedTypeReference<List<ScbTestBook>>>any())
            .getBody())
            .thenReturn(fixtures.scbTestBookList);

        when(restTemplate
            .exchange(
                eq("https://scb-test-book-publisher.herokuapp.com/books/recommendation"),
                any(), any(), ArgumentMatchers.<ParameterizedTypeReference<List<ScbTestBook>>>any())
            .getBody())
            .thenReturn(fixtures.scbRecommendedBookList);

        List<Book> books = cut.getAllSorted();

        Optional<Book> bookId1 = books.stream().filter(book -> book.getId().equals(1L)).findAny();
        Optional<Book> bookId2 = books.stream().filter(book -> book.getId().equals(2L)).findAny();
        Optional<Book> bookId3 = books.stream().filter(book -> book.getId().equals(3L)).findAny();

        // Correct recommendation
        assertThat(bookId1.isPresent()).isTrue();
        assertThat(bookId1.get().getIsRecommended()).isFalse();
        assertThat(bookId2.isPresent()).isTrue();
        assertThat(bookId2.get().getIsRecommended()).isTrue();
        assertThat(bookId3.isPresent()).isTrue();
        assertThat(bookId3.get().getIsRecommended()).isTrue();

    }

    @Test
    public void getAllSortedShouldReturnAllBookWithCorrectSorting() {
        when(restTemplate
            .exchange(
                eq("https://scb-test-book-publisher.herokuapp.com/books"),
                any(), any(), ArgumentMatchers.<ParameterizedTypeReference<List<ScbTestBook>>>any())
            .getBody())
            .thenReturn(fixtures.scbTestBookList);

        when(restTemplate
            .exchange(
                eq("https://scb-test-book-publisher.herokuapp.com/books/recommendation"),
                any(), any(), ArgumentMatchers.<ParameterizedTypeReference<List<ScbTestBook>>>any())
            .getBody())
            .thenReturn(fixtures.scbRecommendedBookList);

        List<Book> books = cut.getAllSorted();

        assertThat(books.get(0).getId()).isEqualTo(3L);
        assertThat(books.get(1).getId()).isEqualTo(2L);
        assertThat(books.get(2).getId()).isEqualTo(1L);

    }

    @Test
    public void getAllSortedShouldReturnNoDuplicates() {
        when(restTemplate
            .exchange(
                eq("https://scb-test-book-publisher.herokuapp.com/books"),
                any(), any(), ArgumentMatchers.<ParameterizedTypeReference<List<ScbTestBook>>>any())
            .getBody())
            .thenReturn(fixtures.scbTestBookListWithDuplicates);

        when(restTemplate
            .exchange(
                eq("https://scb-test-book-publisher.herokuapp.com/books/recommendation"),
                any(), any(), ArgumentMatchers.<ParameterizedTypeReference<List<ScbTestBook>>>any())
            .getBody())
            .thenReturn(fixtures.scbRecommendedBookList);

        List<Book> books = cut.getAllSorted();

        assertThat(books.size()).isEqualTo(3);
        assertThat(books.get(0)).isNotEqualTo(books.get(1));
        assertThat(books.get(0)).isNotEqualTo(books.get(2));
        assertThat(books.get(1)).isNotEqualTo(books.get(2));
    }

    @Test
    public void findByIDShouldReturnCorrectBook() {

        when(restTemplate
            .exchange(
                eq("https://scb-test-book-publisher.herokuapp.com/books"),
                any(), any(), ArgumentMatchers.<ParameterizedTypeReference<List<ScbTestBook>>>any())
            .getBody())
            .thenReturn(fixtures.scbTestBookList);

        Optional<Book> result = cut.findByID(fixtures.bookID);

        assertThat(result.isPresent()).isTrue();
        assertThat(result.get().getId()).isEqualTo(fixtures.bookID);
    }

    @Test
    public void findByIDWithIncorrectIDShouldReturnEmptyOptional() {

        when(restTemplate
            .exchange(
                eq("https://scb-test-book-publisher.herokuapp.com/books"),
                any(), any(), ArgumentMatchers.<ParameterizedTypeReference<List<ScbTestBook>>>any())
            .getBody())
            .thenReturn(fixtures.scbTestBookList);

        Optional<Book> result = cut.findByID(fixtures.notExistingBookID);

        assertThat(result.isPresent()).isFalse();
    }

    class Fixtures {
        private Long bookID = 1L;
        private Long notExistingBookID = 9999999L;
        private List<ScbTestBook> scbTestBookList = asList(
            new ScbTestBook(1L, "Author1", "Cat", 100.05),
            new ScbTestBook(2L, "Author2", "Bat", 100.05),
            new ScbTestBook(3L, "Author3", "Alice", 100.05)
        );
        private List<ScbTestBook> scbRecommendedBookList = asList(
            scbTestBookList.get(1),
            scbTestBookList.get(2)
        );
        private List<ScbTestBook> scbTestBookListWithDuplicates = asList(
            new ScbTestBook(1L, "Author1", "Cat", 100.05),
            new ScbTestBook(2L, "Author2", "Bat", 100.05),
            new ScbTestBook(3L, "Author2", "Bat", 100.05),
            new ScbTestBook(4L, "Author2", "Bat", 100.05),
            new ScbTestBook(5L, "Author3", "Alice", 100.05),
            new ScbTestBook(6L, "Author3", "Alice", 100.05)
        );
    }
}