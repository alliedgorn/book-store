package com.wutthikorn.bookstore.service;

import com.wutthikorn.bookstore.domain.Order;
import com.wutthikorn.bookstore.domain.OrderLine;
import com.wutthikorn.bookstore.domain.User;
import com.wutthikorn.bookstore.repository.OrderRepository;
import com.wutthikorn.bookstore.repository.UserRepository;
import com.wutthikorn.bookstore.view.UserDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {
    private UserServiceImpl cut;

    private Fixtures fixtures = new Fixtures();

    @Mock
    private UserRepository userRepository;

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Before
    public void init() {
        cut = new UserServiceImpl(userRepository, orderRepository, passwordEncoder);
    }

    @Captor
    private ArgumentCaptor<User> captor;

    @Test
    public void createUserShouldEncryptPassword() {

        when(passwordEncoder.encode(fixtures.user.getPassword())).thenReturn(fixtures.encryptedPassword);

        cut.createUser(fixtures.user);

        verify(userRepository).save(captor.capture());
        assertThat(captor.getValue().getPassword()).isEqualTo(fixtures.encryptedPassword);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createUserWithExistingUsernameShouldThrowIllegalArgumentException() {

        when(userRepository.findByUsername(fixtures.user.getUsername())).thenReturn(Optional.of(fixtures.existingUser));

        cut.createUser(fixtures.user);

        verify(userRepository, never()).save(fixtures.user);
    }

    @Test
    public void getUserShouldReturnCorrectUserDto() {

        when(userRepository.findByUsername(fixtures.existingUser.getUsername())).thenReturn(Optional.of(fixtures.existingUser));
        when(orderRepository.findByUserID(fixtures.existingUser.getUserID())).thenReturn(fixtures.orders);
        UserDto result = cut.getUser(fixtures.user.getUsername());

        assertThat(result.getName()).isEqualTo(fixtures.existingUser.getName());
        assertThat(result.getSurname()).isEqualTo(fixtures.existingUser.getSurname());
        assertThat(result.getDateOfBirth()).isEqualTo(fixtures.existingUser.getDateOfBirth());
        assertThat(result.getBooks()).contains(1L, 2L);

    }

    @Test
    public void deleteUserShouldAlsoDeleteUsersOrders() {

        when(userRepository.findByUsername(fixtures.existingUser.getUsername())).thenReturn(Optional.of(fixtures.existingUser));
        when(orderRepository.findByUserID(fixtures.existingUser.getUserID())).thenReturn(fixtures.orders);

        cut.deleteUser(fixtures.existingUser.getUsername());

        verify(userRepository).delete(fixtures.existingUser);
        verify(orderRepository).deleteAll(fixtures.orders);

    }

    @Test(expected = UsernameNotFoundException.class)
    public void deleteUserWithNonExistingUsernameShouldThrowUsernameNotFoundException() {

        when(userRepository.findByUsername(fixtures.existingUser.getUsername())).thenReturn(Optional.empty());

        cut.deleteUser(fixtures.existingUser.getUsername());

    }

    private class Fixtures {
        private User existingUser = new User(1L, "username", "password", "name", "surname", LocalDate.of(1987, 11, 17));
        private User user = new User(1L, "username", "password", "name", "surname", LocalDate.of(1987, 11, 17));
        private String encryptedPassword = "encrypted";
        private List<Order> orders = new ArrayList<>();
        private Order order = new Order();
        private OrderLine orderLine1 = new OrderLine();
        private OrderLine orderLine2 = new OrderLine();

        Fixtures() {
            order.setOrderID(1L).setUserID(1L);
            orderLine1.setName("Book1").setBookID(1L).setPrice(BigDecimal.valueOf(502.43)).setOrderLineID(1L);
            orderLine2.setName("Book2").setBookID(2L).setPrice(BigDecimal.valueOf(231.43)).setOrderLineID(2L);
            order.setOrderLines(asList(orderLine1, orderLine2));
            orders.add(order);
        }
    }
}
