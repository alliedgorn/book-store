package com.wutthikorn.bookstore.service;

import com.wutthikorn.bookstore.domain.User;
import com.wutthikorn.bookstore.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDate;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticationServiceImplTest {

    private AuthenticationServiceImpl cut;

    private Fixtures fixtures = new Fixtures();

    @Mock
    private UserRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Before
    public void init() {
        cut = new AuthenticationServiceImpl(userRepository, passwordEncoder);
    }

    @Test
    public void checkWithCorrectCredentialsShouldReturnTrue() {

        when(userRepository.findByUsername(fixtures.username)).thenReturn(Optional.of(fixtures.user));
        when(passwordEncoder.matches(fixtures.password, fixtures.user.getPassword())).thenReturn(true);

        Boolean result = cut.check(fixtures.username, fixtures.password);

        assertThat(result).isTrue();
    }

    @Test
    public void checkWithIncorrectCredentialsShouldReturnFalse() {

        when(userRepository.findByUsername(fixtures.username)).thenReturn(Optional.of(fixtures.user));
        when(passwordEncoder.matches(fixtures.incorrectPassword, fixtures.user.getPassword())).thenReturn(false);

        Boolean result = cut.check(fixtures.username, fixtures.incorrectPassword);

        assertThat(result).isFalse();
    }

    @Test(expected = UsernameNotFoundException.class)
    public void checkWithIncorrectUsernameShouldThrowUsernameNotFound() {

        when(userRepository.findByUsername(fixtures.username)).thenReturn(Optional.empty());
        cut.check(fixtures.username, fixtures.password);

    }

    private class Fixtures {
        private String username = "john.doh";
        private String password = "password";
        private String incorrectPassword = "incorrectPassword";

        private User user = new User(1L, "john.doh", "encodedPassword", "john", "doh", LocalDate.of(1987, 11, 17));
    }

}