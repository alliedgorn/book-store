package com.wutthikorn.bookstore.service;

import com.wutthikorn.bookstore.domain.Book;
import com.wutthikorn.bookstore.repository.BookRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.not;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class BookServiceImplTest {


    private BookServiceImpl cut;

    private Fixtures fixtures = new Fixtures();

    @Mock
    private BookRepository bookRepository;

    @Before
    public void init() {
        cut = new BookServiceImpl(bookRepository);
    }

    @Test
    public void findAllShouldReturnsBookList() {

        when(bookRepository.getAllSorted()).thenReturn(fixtures.books);
        List<Book> result = cut.getAll();

        assertThat(result, not(empty()));
    }


    private class Fixtures {
        private List<Book> books = asList(
            new Book(1L, "Before We Were Yours: A Novel", "Lisa Wingate", 340.00, false),
            new Book(2L, "When Never Comes", "Barbara Davis", 179.00, true)
        );
    }
}