package com.wutthikorn.bookstore.service;

import com.wutthikorn.bookstore.domain.Book;
import com.wutthikorn.bookstore.domain.User;
import com.wutthikorn.bookstore.exception.ResourceNotFoundException;
import com.wutthikorn.bookstore.repository.BookRepository;
import com.wutthikorn.bookstore.repository.OrderRepository;
import com.wutthikorn.bookstore.repository.UserRepository;
import com.wutthikorn.bookstore.view.OrderDto;
import com.wutthikorn.bookstore.view.OrderSuccessDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceImplTest {

    private OrderServiceImpl cut;

    private Fixtures fixtures = new Fixtures();

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private BookRepository bookRepository;

    @Mock
    private UserRepository userRepository;

    @Before
    public void init() {
        cut = new OrderServiceImpl(orderRepository, bookRepository, userRepository);
    }

    @Test(expected = UsernameNotFoundException.class)
    public void orderWithIncorrectUsernameShouldThrowUsernameNotFoundException() throws ResourceNotFoundException {
        when(userRepository.findByUsername(fixtures.username)).thenReturn(Optional.empty());

        cut.order(fixtures.orderDto, fixtures.username);
    }

    @Test
    public void orderShouldReturnOrderSuccessDtoWithCorrectPrice() throws ResourceNotFoundException {

        when(userRepository.findByUsername(fixtures.username)).thenReturn(Optional.of(fixtures.user));
        when(bookRepository.findByID(fixtures.orderDto.getOrders().get(0))).thenReturn(Optional.of(fixtures.book1));
        when(bookRepository.findByID(fixtures.orderDto.getOrders().get(1))).thenReturn(Optional.of(fixtures.book2));

        OrderSuccessDto result = cut.order(fixtures.orderDto, fixtures.username);

        assertThat(result.getPrice()).isEqualTo(BigDecimal.valueOf(fixtures.book1.getPrice()).add(BigDecimal.valueOf(fixtures.book2.getPrice())));
    }

    @Test(expected = ResourceNotFoundException.class)
    public void orderWithIncorrectBookIDShouldThrowResourceNotFoundException() throws ResourceNotFoundException {
        when(userRepository.findByUsername(fixtures.username)).thenReturn(Optional.of(fixtures.user));
        when(bookRepository.findByID(fixtures.orderDto.getOrders().get(0))).thenReturn(Optional.empty());
        when(bookRepository.findByID(fixtures.orderDto.getOrders().get(1))).thenReturn(Optional.of(fixtures.book2));

        cut.order(fixtures.orderDto, fixtures.username);
    }


    private class Fixtures {
        String username = "username";
        User user = new User(1L, "username", "password", "name", "surname", LocalDate.of(1987, 11, 17));
        OrderDto orderDto = new OrderDto(asList(1L, 2L));
        List<Book> bookList = new ArrayList<>();
        Book book1 = new Book(1L, "Book1", "Author1", 100.05, true);
        Book book2 = new Book(2L, "Book2", "Author2", 30.75, true);

        Fixtures() {
            bookList.add(book1);
            bookList.add(book2);
        }
    }
}
