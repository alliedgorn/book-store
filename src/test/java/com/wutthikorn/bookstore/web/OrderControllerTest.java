package com.wutthikorn.bookstore.web;


import com.wutthikorn.bookstore.service.OrderService;
import com.wutthikorn.bookstore.view.OrderDto;
import com.wutthikorn.bookstore.view.OrderSuccessDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;

import static java.util.Arrays.asList;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(OrderController.class)
@ContextConfiguration
public class OrderControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private OrderService orderService;

    private Fixtures fixtures = new Fixtures();

    @Before
    public void init() {
        mockMvc = MockMvcBuilders
            .standaloneSetup(new OrderController(orderService))
            .build();
    }

    @Test
    public void postOrderShouldReturnPrice() throws Exception {
        when(orderService.order(fixtures.orderDto, fixtures.username)).thenReturn(fixtures.orderSuccessDto);

        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken("username", null);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);

        mockMvc.perform(
            post("/api/users/orders")
                .principal(testingAuthenticationToken)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(fixtures.orderBody))
            .andExpect(status().isOk())
            .andExpect(content().contentType("application/json;charset=UTF-8"))
            .andExpect(jsonPath("$.price").value(fixtures.orderPrice));
    }

    @Test
    public void postOrderWithInvalidParametersShouldReturn400() throws Exception {

        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken("username", null);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);

        mockMvc.perform(
            post("/api/users/orders")
                .principal(testingAuthenticationToken)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(fixtures.badOrderBody))
            .andExpect(status().isBadRequest());
    }

    private class Fixtures {
        String username = "username";
        String badOrderBody = "{\"orders\": []}";
        String orderBody = "{\"orders\": [1,2]}";
        OrderDto orderDto = new OrderDto(asList(1L, 2L));
        BigDecimal orderPrice = new BigDecimal(105.50);
        OrderSuccessDto orderSuccessDto = new OrderSuccessDto(orderPrice);
    }

}
