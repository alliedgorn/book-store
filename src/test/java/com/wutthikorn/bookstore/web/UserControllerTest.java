package com.wutthikorn.bookstore.web;

import com.wutthikorn.bookstore.service.UserService;
import com.wutthikorn.bookstore.view.UserDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;

import static java.util.Arrays.asList;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private UserService userService;

    private Fixtures fixtures = new Fixtures();

    @Before
    public void init() {
        mockMvc = MockMvcBuilders
            .standaloneSetup(new UserController(userService))
            .build();
    }

    @Test
    public void callingPostMethodShouldResponse200() throws Exception {
        mockMvc.perform(post("/api/users")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(fixtures.createBody)
        ).andExpect(status().isOk());
    }

    @Test
    public void callingPostMethodWithIncorrectBodyShouldResponse4xx() throws Exception {
        mockMvc.perform(post("/api/users")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(fixtures.falseCreateBody)
        ).andExpect(status().is4xxClientError());
    }

    @Test
    public void callingGetMethodShouldResponse200WithUserAndOrderedBook() throws Exception {

        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(fixtures.username, null);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);

        when(userService.getUser(fixtures.username)).thenReturn(fixtures.userDto);

        mockMvc.perform(get("/api/users")
            .principal(testingAuthenticationToken)
            .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk())
            .andExpect(content().contentType("application/json;charset=UTF-8"))
            .andExpect(jsonPath("$.name").value("john"))
            .andExpect(jsonPath("$.surname").value("doe"))
            .andExpect(jsonPath("$.date_of_birth").value("15/01/1985"))
            .andExpect(jsonPath("$.books[0]").value(1))
            .andExpect(jsonPath("$.books[1]").value(4));
    }

    @Test
    public void callingDeleteMethodShouldReturn200() throws Exception {

        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(fixtures.username, null);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);

        mockMvc.perform(delete("/api/users")
            .principal(testingAuthenticationToken)
        ).andExpect(status().isOk());

    }

    private class Fixtures {
        String createBody = "{\"username\": \"john.doe\", \"password\": \"thisismysecret\", \"date_of_birth\": \"15/01/1985\"}";
        String falseCreateBody = "{\"username\": \"john.doe\", \"date_of_birth\": \"15/01/1985\"}";
        String username = "john.doe";
        UserDto userDto = new UserDto()
            .setName("john")
            .setSurname("doe")
            .setDateOfBirth(LocalDate.of(1985, 1, 15))
            .setBooks(asList(1L, 4L));

    }
}
