package com.wutthikorn.bookstore.web;

import com.wutthikorn.bookstore.domain.Book;
import com.wutthikorn.bookstore.service.BookService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static java.util.Arrays.asList;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(BookController.class)
public class BookControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private BookService bookService;

    private Fixtures fixtures = new Fixtures();

    @Before
    public void init() {
        mockMvc = MockMvcBuilders
            .standaloneSetup(new BookController(bookService))
            .build();
    }

    @Test
    public void callingGetMethodShouldReturnsBookList() throws Exception {
        when(bookService.getAll()).thenReturn(fixtures.books);
        mockMvc.perform(get("/api/books").accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentType("application/json;charset=UTF-8"))
            .andExpect(jsonPath("$[0].id").value(1))
            .andExpect(jsonPath("$[0].name").value("Before We Were Yours: A Novel"))
            .andExpect(jsonPath("$[0].author").value("Lisa Wingate"))
            .andExpect(jsonPath("$[0].price").value(340.00))
            .andExpect(jsonPath("$[0].is_recommended").value(false))
            .andExpect(jsonPath("$[1].id").value(2))
            .andExpect(jsonPath("$[1].name").value("When Never Comes"))
            .andExpect(jsonPath("$[1].author").value("Barbara Davis"))
            .andExpect(jsonPath("$[1].price").value(179.00))
            .andExpect(jsonPath("$[1].is_recommended").value(true));
    }

    private class Fixtures {
        private List<Book> books = asList(
            new Book(1L, "Before We Were Yours: A Novel", "Lisa Wingate", 340.00, false),
            new Book(2L, "When Never Comes", "Barbara Davis", 179.00, true)
        );
    }
}
